app.controller("ctrlCheckout", function($scope, $http) {
  $scope.checkout = {};

  $http.get('/csrf_token', $scope.checkout).then(function (res) {
    $scope.checkout.csrf_token = res.data
  })

  $scope.message = "please enter location and submit!";
  $scope.form_hidden = false
  $scope.submitCheckout = function() {
    $http.post('/checkout', $scope.checkout).then(function (res) {
        $scope.message= res.data
    })
  };
});
